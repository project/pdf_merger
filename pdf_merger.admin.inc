<?php
/**
 * @file
 * Contains the administrative functions of the PDF merger module.
 *
 * This file is included by the PDF merger module, and includes the
 * settings form.
 */

/**
 * Admin configure form control on page.
 */
function pdf_merger_config() {
  if (pdf_merger_library_exist()) {
    $form['pdf_merger_directory'] = array(
      '#type' => 'textfield',
      '#size' => 50,
      '#title' => t('PDF Located Directory'),
      '#default_value' => variable_get('pdf_merger_directory', 'sites'),
      '#description' => t('Please specify the directory to get the pdf file to merge'),
    );
    $form['pdf_merger_save_option'] = array(
      '#type' => 'radios',
      '#title' => t('PDF Option in'),
      '#options' => array(
        t('Web Browser'),
        t('Save Dialog Box'),
        t('Save files in directory'),
      ),
      '#default_value' => variable_get('pdf_merger_save_option', 2),
    );
    $form['pdf_merger_path_redirect'] = array(
      '#type' => 'checkbox',
      '#title' => t('Open the PDF file'),
      '#default_value' => variable_get('pdf_merger_path_redirect', 0),
    );
    $form['pdf_merger_include_merger_directory'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include the Merger Directory PDF file to merge'),
      '#default_value' => variable_get('pdf_merger_include_merger_directory', 0),
    );
  }
  else {
    $form['settings'] = array(
      '#type' => 'markup',
      '#value' => '<p>' . t('No PDF Merging tool found! Please dowload PDF merger supported library from !library_url and placed under the libraries (sites/all/libraries).', array(
          '!library_url' => l(t('URL'), 'http://pdfmerger.codeplex.com/', array('attributes' => array('target' => '_blank'))),
      )) . '</p>',
    );
  }
  return system_settings_form($form);
}
