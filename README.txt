MODULE
------
pdf_merger

DESCRIPTION/FEATURES
--------------------
* This module is used to merge pdf's.


REQUIREMENTS
------------
Drupal 7.0


INSTALLATION
------------

Decompress the file into your Drupal modules
directory (usually sites/all/modules).

Enable the pdf_merger module: Administration > Modules (admin/modules)

Please make sure to download the PDFMerger Library
from http://pdfmerger.codeplex.com/ and placed under the libraries
(sites/all/libraries)

Make sure the PDFMerger Library folder contains the fpdf, fpdi library and
PDFMerger.php file

Like the below:
---------------
sites/all/libraries/PDFMerger/fpdf/fpdf.php
sites/all/libraries/PDFMerger/fpdi/fpdi.php
sites/all/libraries/PDFMerger/PDFMerger.php

CONFIGURATION
-------------

- There are several settings that can be configured in the following places:

  Administration > Modules (admin/modules)
    Enable or disable the module. (default: disabled)


To merge the PDF other than this module, just enable the module and call the
below function like the below format

pdf_merge($files, $option="download");

INPUT:
------
$files = array
(
    [mpdf50demo.pdf] => all
    [new_features_4_0.pdf] => all
    [one.pdf] => all
)

KEY   - Name of the PDF file under the specified located PDF directory
VALUE - Enter valid page number to merge the PDF
Page number 1,3,5 OR Page Starts with 5-10 - Option Like print page
DEFAULT value "all"

$option - "browser", "download", "file"

KNOWN ISSUES:
------------

1.Could not able to merge the encrypted PDF file.

2. Some times you are getting exception like
"'Error outputting PDF to 'file" - Please refer this URL
http://pdfmerger.codeplex.com/workitem/7390 OR

To resolve the above mentioned error

Change the line 118 in sites/all/libraries/PDFMerger/PDFMerger.php
FROM
  if($fpdi->Output($outputpath, $mode))
TO
  if($fpdi->Output($outputpath, $mode) == '')


LATEST CHANGES:
---------------

1.Now provide an option to merge the PDF in the whole sites directory.

2.Add hook_form_validate to validate the form data.

3.Add hook_uninstall  to delete the module specific variable.

4.Now PDF "configure" and "Merge" acts as tab menu (admin/settings/pdfmerge).
